# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du contenu éditorial du site nnjzz.xunga.org

basé sur [Hugo](https://gohugo.io/), [Decap](https://decapcms.org) et [GitLab](https://gitlab.com).

## utilisation

- front-end : https://nnjzz.xunga.org/
- gestion du contenu : https://nnjzz.xunga.org/admin/

## gestion des accès

L'accès à la gestion du contenu se fait au moyen d'un compte [GitLab](https://gitlab.com/users/sign_in).  
L'utilisateur doit avoir au minimum un rôle _Maintainer_ sur le projet [Non_Jazz](https://gitlab.com/crachecode/non_jazz) ou le groupe [crachecode](https://gitlab.com/crachecode).
