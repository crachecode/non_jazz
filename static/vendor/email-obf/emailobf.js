class EmailObf extends HTMLElement {
    constructor() {
        super()

        let shadow = this.attachShadow({mode: 'open'})

        let wrapper = document.createElement('span')
        wrapper.style.userSelect = 'none'

        let domain = document.createElement('span')
        domain.style.unicodeBidi = 'bidi-override'
        domain.style.direction = 'rtl'

        let str = this.innerHTML
        let strings = str.split('|')
        let local = document.createTextNode(this.rot13(strings[0])+'@')
        domain.textContent = strings[1]
        let extension = document.createTextNode('.'+strings[2])

        wrapper.appendChild(local)
        wrapper.appendChild(domain)
        wrapper.appendChild(extension)

        shadow.appendChild(wrapper)
    }

    rot13(str) {
        return str.replace(/[a-zA-Z]/g,(c) => {
            return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26)
        })
    }
}

customElements.define('email-obf', EmailObf)
