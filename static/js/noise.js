import { createNoise3D } from "https://cdn.skypack.dev/simplex-noise@4.0.0"

let patternSize = 1
let grainWidth = 400
let frameLength = 1
let ratio
const contrast = 5
const luminosity = 0
const fps = 30
const mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)

document.addEventListener('DOMContentLoaded', () => {
  /*let noisy = noise(.2,.4,.3)
  console.log(noisy)*/

  const noise3D = createNoise3D()
  const canvas = document.getElementById('wallpaper')
  const ctx = canvas.getContext('2d')
  const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
  const data = imageData.data
  let t = 0

  function drawPlasma(){
    for (let x = 0; x < 256; x++) {
      for (let y = 0; y < 256; y++) {
        let r = noise3D(x/grainWidth, y/patternSize, t/frameLength) * contrast + luminosity
        data[(x + y * 256) * 4 + 0] = r * 255
        data[(x + y * 256) * 4 + 1] = r * 255
        data[(x + y * 256) * 4 + 2] = r * 255
        data[(x + y * 256) * 4 + 3] = 255
      }
    }
    t++
    ctx.putImageData(imageData, 0, 0)
    setTimeout(() => {
      requestAnimationFrame(drawPlasma)
    }, 1000 / fps)
  }
  if(!mobile || window.location.pathname == '/'){
    drawPlasma()
  }
  if (mobile && window.location.pathname == '/'){
    setTimeout(() => {
      document.querySelector('#nnjzz > a').style.opacity = 1
    }, 2000)
  }
  let nnjzz = document.getElementById('nnjzz')
  if (nnjzz !== null) {
    document.getElementById('nnjzz').addEventListener('click', (e) => {
      let el = e.target
      window.location = el.getAttribute("href")
    })
  }
})

let mouseX
let mouseY

document.addEventListener('mousemove', e => {
	mouseX = e.clientX
	mouseY = e.clientY
  //console.log('mouseX :', mouseX);console.log('mouseY :', mouseY)
  if (window.location.pathname == '/'){
    frameLength = Math.pow(Math.abs((mouseY/window.innerHeight)-.5)*4, 11)+2
  }
  else {
    frameLength = (Math.abs((mouseY/window.innerHeight)-.5)*1000)+1
  }
  grainWidth = Math.pow((mouseX - (window.innerWidth/2))/window.innerWidth*8, 8)+1
  patternSize = Math.pow((mouseX - window.innerWidth)/window.innerWidth*8, 4)
  //console.log('frameLength : ', frameLength,'/ grainWidth', grainWidth,'/ patternSize', patternSize)
})
