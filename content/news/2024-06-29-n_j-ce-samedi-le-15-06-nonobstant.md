---
title: ^N_J CE SAMEDI LE 15.06 NONOBSTANT
draft: false
date: 2024-06-15T20:00:00.000Z
place: Treize 24 rue Moret 75011 M° Couronnes
image: /img/240615-treize.jpg
admin_show: true
---
SCHOCO MUNE jp /nl
ELISE EHRY & KITTY MARIA fr / nl
AUGUSTÉ VICKUNAITÉ lt
L'ARGENT fr

chez
Treize
24 rue Moret
75011M° Couronnes

 20:00 portes
20h40 action

P.A.F. 6€

**SCHOCO MUNE** jp
' playing sounds without thoughts and manner '
Musicienne / performeuse / improvisatrice ( électronique / noise ) nipponnevivant à Amsterdam.Micros contact / feedback / electronics.Expect the unexpected.À noter qu'elle est aussi co-organisatrice de SOTU Festival chaque avril  à Amsterdam. Elle s'occupe également d'une série de concerts sous le nom de " kinky noise " et en novembre d'une autre série connue sous le nom de " Noisevember ".

https://peertube.groenestam.nl/videos/watch/477d0650-3b61-4e4f-8ae7-05457151d5bc\
https://soundcloud.com/schocomune\
https://peertube.groenestam.nl/videos/watch/aa7125b6-c5e6-471a-a843-30b07ee9beaa\
https://peertube.groenestam.nl/videos/watch/59df184a-700d-4c30-a4d2-317d7a4a3397\
https://www.instagram.com/schocomune\
https://peertube.groenestam.nl/videos/watch/477d0650-3b61-4e4f-8ae7-05457151d5bc\
https://soundcloud.com/schocomune

**ELISE EHRY & KITTY MARIA** fr / nl
sont hôtesses de l’air sans emploi et collègues dans la même compagnie aérienne franco-néerlandaise qu’elles ont créée en 2014: Hosting Air.\
En dépliant la figure de l’hôtesse de l’air, elles incarnent des modalités de retrait du monde du travail afin d’être en permanence off-duty.\
Puisque l’absence de fonction les rend inutiles, elles remettent en question l’acte même de performer  ? Que signifie ce retrait du monde du travail pour rejoindre celui de la performance?\
Pour consolider ce retrait, elles spéculent sur le concept de «performance non-performative».\
Air tonalities est une performance sonore qu’elles développent actuellement et bien que n’ayant pas l’oreille musicale, elles entendent façonner leurs propres instruments en argile et petits circuits électroniques.\
Elles ne possèdent pas de lien Bandcamp, mais on peut voir des choses par là:

https://www.instagram.com/unemployed_airhostesses/\
https://www.youtube.com/watch?v=aoxl9SGqZqY\
https://www.instagram.com/unemployed_airhostesses/\
https://www.youtube.com/watch?v=aoxl9SGqZqY

**AUGUSTÉ VICKUNAITÉ** lt\
Performeuse / collagiste sonore, avec une formation en sciences physiques ( ! ).

Nous l'avons déjà accueillie il y a quelques semaines et - une fois n'est pas coutume - l'invitons à nouveau à si peu d'intervalle.

Elle utilise principalement des magnétophones à bandes pour jouer, enregistrer et créer des compositions instantanées à base de d'enregistrementsde terrain divers, dysfonctionnements de la technologie vintage ou encore des sons d'instruments enregistrés dans des environnements naturels.

Sa performance contiendra des boucles de bandes audio et des collages effectués à partir de de bandes trouvées.

https://www.youtube.com/watch?v=bvRfxetWxvc&\
https://archive.org/details/@augustevi\
https://augustevickunaite.bandcamp.com/\
https://soundcloud.com/skyg1\
https://www.youtube.com/watch?v=bvRfxetWxvc\
https://archive.org/details/@augustevi\
https://augustevickunaite.bandcamp.com/\
https://soundcloud.com/skyg1

**L'ARGENT** fr\
proto quelque chose

"C'est mon projet commercial pour faire quelques cachets"

No no gagabber mutuelle de nouveau oui oui "

Nouveau projet de Thomas Dunoyer ( Mamiedaragon,
No Lagos Musique... ).

Accumulation / soustraction de boucles / samples no dub.

https://pourlargent.bandcamp.com/album/largent-le-roi-cible

Fly - Jo L'Indien
