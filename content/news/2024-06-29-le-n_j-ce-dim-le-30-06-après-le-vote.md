---
title: LE N_J CE DIM LE 30.06 APRÈS LE VOTE
draft: false
date: 2024-06-30T19:00:00.000Z
place: La Générale
image: /img/30.06.jpg
admin_show: true
---
BONJOUR / BONSOIR / / HELLOOO)))

Le Non_Jazz

DIMANCHE LE 30.06

**MARIJA KOVAČEVIĆ** srb\
**JANNEKE VAN DER PUTTEN** nl\
**GASKIÈRE** fr / ar\
**PAULA SANCHEZ & LEANDRO BARZABAL** ar

![Dessin - Emma Kerssenbrock](/img/marija.jpg)

**MARIJA KOVAČEVIĆ** srb nyc\
Solo pour un assortiment de violons cassés / acoustiques ou amplifiés /  on verra bien.

Il y a environ quatre ans, alors qu'elle travaillait comme prof de violon à la Brooklyn Music School, elle est un jour tombée sur un stock de violons cassés qu'elle a récupérés en se disant que ces instruments mis au rebut pourraient avoir une " vie après la mort " et servir de terrain de jeu et d'expérimentation par / pour des non-musiciens. Et puis, elle s'est progressivement prise au jeu elle-même, en découvrant rapidement tout un monde, étrange et hétérogène,\
riche d'un potentiel sonore nouveau et imprévisible grâce à ces objets cassés, " désapprouvés ", si fragiles et si précieux autrefois, du temps de leur
" splendeur ".

Apprenant à travailler avec et à partir de chaque violon et archet ainsi
" dysfonctionnels ", approchés différemment, individuellement, elle a ainsi développé un vocabulaire à part, idiosyncratique, en explorant les possibilités spécifiques de chaque instrument, en en  tirant les cordes, en jouant des / avec des fissures, en faisant grincer les éclisses et en tordant les chevalets.

Born in Yugoslavia, based in New York, Marija works in classical, experimental and improvised music and theater.\
Performing on broken violins, the instruments come to seem less like a violin that has seen better days than another sort of instrument altogether,\
a species that scuttled off the evolutionary train and bred shamelessly under a bridge.

They are not what they once were but they have to keep going. "

[youtube.com/watch?v=Xi2En1AAeVs](https://www.youtube.com/watch?v=Xi2En1AAeVs)

[youtube.com/watch?v=bN37_jDm9W4](https://www.youtube.com/watch?v=bN37_jDm9W4)

[marijakovacevic.bandcamp.com/album/music-for-broken-violins-vol-1](https://marijakovacevic.bandcamp.com/album/music-for-broken-violins-vol-1)

[www.mkovacevic.com](https://www.mkovacevic.com/)

**JANNEKE VAN DER PUTTEN** nl rotterdam\
Sa pratique implique des expériences d'écoute, de performance, de son et de vidéo, de textile et de peinture, des ateliers, des projets musicaux et la création de plateformes d'échange culturel.

Sa voix est son principal outil, la guidant à travers des explorations physiques dans différents paysages et architectures.\
Autodidacte dans diverses techniques vocales étendues, elle a également une formation en chant classique Dhrupad de l'Inde du Nord.

Plusieurs expositions / installations / performances à travers le monde à son actif.Collaborations, notamment avec Siavash Akhlaghi (IR), Budhaditya Chattopadhyay (IN), Werner Durand (DE), Philemon Mukarno (ID/NL), Rory Pilgrim (UK), Marcus Schmickler (DE) et Lucie Vítková (CZ). 

https://www.hear.fr/janneke-van-der-putten/

https://jannekevanderputten.nl/

https://aloardi.bandcamp.com/album/jnnk-2023

https://www.youtube.com/watch?v=I-oYUMLdUqo

https://www.youtube.com/watch?v=xz0fw-14Vl0

**PAULA SANCHEZ & LEANDRO BARZABAL** ar\
Duo inédit - me semble-t-il ? - entre ces deux compatriotes : l'une basée en Suisse, l'autre,Parisien d'adoption depuis de nombreuses années, et bien repéré dans moult soirées impro-noise-interlopes d'ici.

Tous les deux " semblent " bien prolifiques et bien porté.e.s sur l'aspect performatif de leurs ... performances,tant au niveau d'accessoires qu'au niveau (im)purement sonore.

Travaillant à l'intersection de l'improvisation libre / musique expérimentale / art / performance ( composition / décomposition /  destruction / construction ),\
Paula Sanchez" augmente " son instrument, en recourant aux techniques dites élargies ( utilisant des boutsde plastique / verre / végétaux... ), sans rechigner à y ajouter de l'électronique ou de la voix.

Plusieurs projets et collaborations ( avec Fred Frith, Mariana Carvalho, Violeta Garcia, Kevin Sommer... )

Leandro ( " que-l'on-ne-le-présente-plus " ) est (non)guitariste, performiste, contorsionniste, body-buildeur et bidouilleur de sons et de dispositifs plus ou moins électro-acoustiques, qu'il s'acharne à déconstruire à re-modeler et détourner encore davantage à chaque utilisation et à chaque réappropriation.

Concoctions, déjections, oripeaux sonores / commotions sensorielles et j'en passe des meilleures.

https://www.youtube.com/watch?v=D__uEuo3QZk
https://www.youtube.com/watch?v=L27dKMDu4xM

[mme-sanchez.bandcamp.com](https://mme-sanchez.bandcamp.com/)

[youtube.com/watch?v=L5JmyBjjmzI](https://www.youtube.com/watch?v=L5JmyBjjmzI)

[youtube.com/watch?v=Wu0tbE9Xc_8](https://www.youtube.com/watch?v=Wu0tbE9Xc_8)

[youtube.com/watch?v=8PMkfae_ivI](https://www.youtube.com/watch?v=8PMkfae_ivI)

[youtube.com/watch?v=EPFsmI26c24](https://www.youtube.com/watch?v=EPFsmI26c24)

**GASKIÈRE** fr / ar\
" Il arrive parfois qu'une graine s'envole au gré du vent et se plante à des endroits insolites.Ainsi naquit Gaskière. Une jachère qui évolue constamment pour mieux repousser. Ce trio d'improvisation est à l'initiative de Roberto Robao, Emma Kerssenbrock et David Lantran.\
Mycélium hautement contagieux, il répand ses spores pour mieux muter et vous entraîne dans une valse inconstante."

Voix, trucs, électronique, violoncelle.
Encore une de ces formidables surprises issues de la cuisse de Jupiter des soirées BROKEN IMPRO.

Si je ne m'abuse !

[gaskiere.bandcamp.com/album/totale-gaskiere](https://gaskiere.bandcamp.com/album/totale-gaskiere)

Dessin - Emma Kerssenbrock

Après le VOTE 

Le Non_Jazz\
**NB / EARLY SHOW!**

**19H** Portes\
**19H30** ACTION!

Nous ne pouvons pas afficher cette adresse via les réseaux sociaux
/ MERCI de ne pas le faire non plus
.

**No CB** / **MERCI** de prévoir du cash pour la P.A.F.\
( libre néanmoins\
6€ WHY NOT / ou plus WHY NOT ) ]\
ainsi que pour la buvette.

Ordre de passage incertain mais il est fort probable que nous commencions par\
GASKIERE\
à **19H30**\
donc.

Fly - **Jo L'Indien**
